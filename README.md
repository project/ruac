## INTRODUCTION
URL Access Control
Secure your site with URL Access Control: easily restrict direct access to
specified URLs after enabling and configuring them.

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

Install the module as you would any other contributed Drupal module. 
Refer to the Installing Modules documentation for detailed instructions.

## CONFIGURATION
To use:
1. Navigate to Administration > Extend > and Search for
   URL Access Control Module.
2. Enable the URL Access Control Module.
3. Add a URL to the configuration form that you wish to restrict direct
   access for users.
4. After saving a URL in the configuration form, if you attempt to access it
   directly, you will receive an "Access Denied" message. However, you can 
   access the same URL when navigating through the site.
5. You can create your own custom Access Denied page and set its path in
   Configuration > Basic site settings > Error pages > Default 403
   (access denied) page. Here, you can specify your custom Access Denied
   page. If no custom page is set, the default page created in the module
   will be used.

## MAINTAINERS

Current maintainers:

- Pankaj kumar  - (https://www.drupal.org/u/pankaj_lnweb)
- Shikha Dawar  - (https://www.drupal.org/u/shikha_lnweb)
- Vivek kumar  - (https://www.drupal.org/u/vivek_lnwebworks)
- Praveen Rani -  (https://www.drupal.org/u/praveen rani)
