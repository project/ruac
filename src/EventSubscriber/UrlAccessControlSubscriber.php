<?php

namespace Drupal\url_access_control\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber to restrict access to specific URLs.
 */
class UrlAccessControlSubscriber implements EventSubscriberInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new UrlAccessControlSubscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Subscribe to the KernelEvents::REQUEST event with priority 30.
    $events[KernelEvents::REQUEST][] = ['onKernelRequest', 30];
    return $events;
  }

  /**
   * Responds to kernel request events.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   */
  public function onKernelRequest(RequestEvent $event) {
    // Get the URL Access Control settings from configuration.
    $config = $this->configFactory->get('url_access_control.settings');
    $urls = $config->get('urls') ?? [];
    $request = $event->getRequest();

    // Get the path of the current request.
    $path = $request->getPathInfo();

    // Check if the current request path matches any of the restricted URLs.
    foreach ($urls as $restricted_url) {
      if ($restricted_url && strpos($path, $restricted_url) !== FALSE) {
        // Check if the Referer header is present.
        $referer = $request->headers->get('referer');
        if ($referer) {
          // Allow access since the Referer is from the same site.
          return;
        }
        else {
          // Get the configured 403 access denied page path.
          $site_config = $this->configFactory->get('system.site');
          $default_403_path = $site_config->get('page.403');

          if ($default_403_path) {
            // Convert to absolute URL.
            $page_403_url = Url::fromUri('internal:' . $default_403_path)
              ->setAbsolute()
              ->toString();
          }
          else {
            // Redirect to custom access denied page route.
            $url = Url::fromRoute('url_access_control.access_denied');
            $page_403_url = $url->toString();
          }

          // Redirect to determined 403 page URL.
          $redirect_response = new RedirectResponse($page_403_url);
          $event->setResponse($redirect_response);

          return;
        }
      }
    }
  }

}
