<?php

namespace Drupal\url_access_control\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides the Access Denied page controller.
 *
 * This controller is responsible for rendering the Access Denied page
 * when a user attempts to access a restricted URL directly.
 */
class AccessDeniedController extends ControllerBase {

  /**
   * Renders the Access Denied page.
   *
   * @return array
   *   A render array representing the Access Denied page.
   */
  public function accessDenied() {
    return [
      '#theme' => 'access_denied_page',
      '#attached' => [
        'library' => [
          'url_access_control/access_denied_page',
        ],
      ],
    ];
  }

}
