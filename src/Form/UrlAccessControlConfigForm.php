<?php

namespace Drupal\url_access_control\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a URL Access Control configuration form.
 */
final class UrlAccessControlConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'url_access_control_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['url_access_control.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Load the current configuration.
    $config = $this->config('url_access_control.settings');

    // Get the list of URLs from the configuration or initialize an empty array.
    $urls = $config->get('urls') ?? [];

    // Form element to edit the list of URLs as a textarea.
    $form['urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t('URLs (One per line)'),
      '#default_value' => implode("\n", $urls),
      '#description' => $this->t('Enter one URL path per line. Absolute URLs will be ignored.'),
      '#required' => TRUE,
    ];

    // Call parent buildForm() to finalize form building.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // Get the value of the 'urls' textarea from the submitted form state.
    $urls = $form_state->getValue('urls');

    // Split the textarea value into lines and check each URL.
    $lines = explode("\n", $urls);
    foreach ($lines as $line) {
      $trimmed_url = trim($line);
      if (!empty($trimmed_url)) {
        // Check if the URL is absolute (starts with http:// or https://).
        if (strpos($trimmed_url, 'http://') === 0 || strpos($trimmed_url, 'https://') === 0) {
          // Set an error if an absolute URL is found.
          $form_state->setErrorByName('urls', $this->t('Absolute URLs are not allowed. 
          Please enter relative URLs only.')
          );
          return;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Get the value of the 'urls' textarea from the submitted form state.
    $urls = explode("\n", $form_state->getValue('urls'));

    // Remove empty lines and trim URLs.
    $urls = array_filter(array_map('trim', $urls));

    // Save the updated list of URLs to the configuration.
    $this->config('url_access_control.settings')
      ->set('urls', $urls)
      ->save();

    // Indicate that the configuration has been saved.
    $this->messenger()->addStatus(
      $this->t('The configuration has been saved.')
    );
  }

}
